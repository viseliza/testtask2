import { News as AppModule } from '@prisma/client';

export class News implements AppModule {
  id: string;
  // owner: string;
  authtor: string;
  content: string;
  social: string | null
}